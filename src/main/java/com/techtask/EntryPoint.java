package com.techtask;

import com.techtask.matrix.BooleanSquareMatrix;
import com.techtask.utils.MatrixUtils;

import java.util.InputMismatchException;
import java.util.Scanner;

public class EntryPoint {

    public static void main(String[] args) {

        int matrixSize = 0;
        Scanner scanner = new Scanner(System.in);

        System.out.println("This is a program for Square Boolean Matrices Multiplication");
        System.out.println("It will generate 2 square matrices for you");
        System.out.print("Choose size of the Matrices for generation (1..10000): ");

        boolean inputOk = false;
        while (!inputOk) {
            try {
                matrixSize = scanner.nextInt();
                if (matrixSize < 1 || matrixSize > 10000) {
                    System.out.println("Unsupported size! I will do this choice for you.\ncom.techtask.matrix.BooleanSquareMatrix size is 500!");
                    matrixSize = 500;
                }
                inputOk = true;
            } catch (InputMismatchException e) {
                System.err.println("Unsupported format! Try again...");
                scanner.nextLine();
            }
        }

        BooleanSquareMatrix a1 = new BooleanSquareMatrix();
        a1.generateSquareMatrix(matrixSize);
        System.out.println("------ Matrix 1 ------ \n" + a1.toString());

        BooleanSquareMatrix a2 = new BooleanSquareMatrix();
        a2.generateSquareMatrix(matrixSize);
        System.out.println("------ Matrix 2 ------ \n" + a2.toString());


        MatrixUtils utils = new MatrixUtils();

        BooleanSquareMatrix a3 = utils.multiply(a1, a2);
        System.out.println("Result com.techtask.matrix.BooleanSquareMatrix using sequential computation: ");
        System.out.println(a3.toString());

        BooleanSquareMatrix a4 = utils.multiplyInParallel(a1, a2);
        System.out.println("Result com.techtask.matrix.BooleanSquareMatrix using parallel computation: ");
        System.out.println(a4.toString());


    }
}
