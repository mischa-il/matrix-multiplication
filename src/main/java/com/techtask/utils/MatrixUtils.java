package com.techtask.utils;

import com.techtask.matrix.BooleanSquareMatrix;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.TimeUnit;

public class MatrixUtils {

    public BooleanSquareMatrix multiply(BooleanSquareMatrix a, BooleanSquareMatrix b) {

        if (!validate(a, b)) return null;

        BooleanSquareMatrix result = new BooleanSquareMatrix();
        boolean[][] tempMatrix = new boolean[a.getContent().length][b.getContent()[0].length];

        System.out.println("Multiply on One Thread started...");

        Long startTime = System.currentTimeMillis();
        multiplyBooleanMatrices(a, b, tempMatrix);
        Long endTime = System.currentTimeMillis();

        result.setContent(tempMatrix);

        System.out.println("Execution time is " + (endTime - startTime) + " milliseconds");

        return result;
    }


    public BooleanSquareMatrix multiplyInParallel(BooleanSquareMatrix a, BooleanSquareMatrix b) {

        if (!validate(a, b)) return null;

        if (a.getContent().length < 4) {
            System.out.println("com.techtask.matrix.BooleanSquareMatrix Multiplication will be run in a single Thread");
            return multiply(a, b);
        }

        System.out.println("Multiply in Parallel started...");

        ExecutorService threadPool = Executors.newFixedThreadPool(Runtime.getRuntime().availableProcessors());
        BooleanSquareMatrix result = new BooleanSquareMatrix();

        boolean[][] tempMatrix = new boolean[a.getContent().length][b.getContent()[0].length];
        Long startTime = System.currentTimeMillis();

        threadPool.execute(new OneThread(a, b, tempMatrix, 0, a.getContent().length / 2));
        threadPool.execute(new OneThread(a, b, tempMatrix, a.getContent().length / 2, a.getContent().length));

        threadPool.shutdown();
        Long endTime = System.currentTimeMillis();

        try {
            threadPool.awaitTermination(Long.MAX_VALUE, TimeUnit.MILLISECONDS);
        } catch (InterruptedException e) {
            System.err.println("Parallel execution was not finished successfully!");
            e.printStackTrace();
        }

        result.setContent(tempMatrix);
        System.out.println("Execution time is " + (endTime - startTime) + " milliseconds");

        return result;
    }

    //We can extend validation, but I's just a demo and I will use matrices generated correctly
    private boolean validate(BooleanSquareMatrix a, BooleanSquareMatrix b) {

        if (a == null || a.getContent() == null || a.getContent().length < 1) {
            System.out.println("First multiplier is empty or null!");
            return false;
        }

        if (b == null || b.getContent() == null || b.getContent().length < 1) {
            System.out.println("Second multiplier is empty or null!");
            return false;
        }

        if (a.getContent().length != b.getContent()[0].length) {
            System.out.println("These matrices can not be multiplied!");
            return false;
        }
        return true;
    }

    private boolean[][] multiplyPartOfBooleanMatrices(BooleanSquareMatrix m1, BooleanSquareMatrix m2, boolean[][] result, int startIndex, int endIndex) {

        for (int x = startIndex; x < endIndex; x++) {
            for (int y = 0; y < m2.getContent()[0].length; y++) {
                result[x][y] = false;
                for (int z = 0; z < m1.getContent().length; z++) {
                    result[x][y] ^= m1.getContent()[x][z] & m2.getContent()[z][y];
                }
            }
        }
        return result;
    }

    private boolean[][] multiplyBooleanMatrices(BooleanSquareMatrix m1, BooleanSquareMatrix m2, boolean[][] result) {

        multiplyPartOfBooleanMatrices(m1, m2, result, 0, m1.getContent().length);
        return result;
    }

    class OneThread implements Runnable {
        BooleanSquareMatrix m1;
        BooleanSquareMatrix m2;
        boolean[][] result;
        int startIndex;
        int endIndex;

        public OneThread(BooleanSquareMatrix m1, BooleanSquareMatrix m2, boolean[][] result, int startIndex, int endIndex) {
            this.m1 = m1;
            this.m2 = m2;
            this.result = result;
            this.startIndex = startIndex;
            this.endIndex = endIndex;
            new Thread(this);
        }

        public void run() {
            multiplyPartOfBooleanMatrices(m1, m2, result, startIndex, endIndex);
        }
    }

}
