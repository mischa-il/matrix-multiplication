package com.techtask.matrix;

import java.util.Random;

public class BooleanSquareMatrix implements ISquareMatrix {

    private boolean[][] content;

    public BooleanSquareMatrix() {
    }

    public IMatrix generateSquareMatrix(int i) {

        boolean[][] matrix = new boolean[i][i];

        Random rnd = new Random();
        for (int x = 0; x < i; x++) {
            for (int y = 0; y < i; y++) {
                matrix[x][y] = rnd.nextBoolean();
            }
        }
        this.content = matrix;
        return this;
    }

    @Override
    public int findDeterminant() {
        //Implement on demand
        return 0;
    }

    @Override
    public String toString() {
        if (this.content.length < 1)
            return "com.techtask.matrix.BooleanSquareMatrix is empty";

        StringBuilder sb = new StringBuilder();

        for (int x = 0; x < content.length; x++) {
            for (int y = 0; y <= content[0].length - 1; y++) {
                sb.append(content[x][y] ? "1 " : "0 ");
            }
            sb.append("\n");
        }
        return sb.toString();
    }

    public boolean[][] getContent() {
        return content;
    }

    public void setContent(boolean[][] content) {
        this.content = content;
    }
}
