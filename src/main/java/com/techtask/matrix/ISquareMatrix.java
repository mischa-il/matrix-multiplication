package com.techtask.matrix;

interface ISquareMatrix extends IMatrix {

    IMatrix generateSquareMatrix(int i);

    default int findDeterminant() {
        //Implement on demand
        return 0;
    }

}
