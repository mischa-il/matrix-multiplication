package com.techtask.matrix;

public interface IMatrix {

    default IMatrix multiplyByNumber(int i) {
        //Implement on demand
        System.out.println("Implement on demand");
        return null;
    }

    default IMatrix transpose() {
        //Implement on demand
        System.out.println("Implement on demand");
        return null;
    }

    default IMatrix findMinor(int i, int j) {
        //Implement on demand
        System.out.println("Implement on demand");
        return null;
    }

    default int findRank() {
        //Implement on demand
        System.out.println("Implement on demand");
        return 0;
    }
}
