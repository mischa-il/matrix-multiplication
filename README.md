# com.techtask.matrix-multiplication

Hi guys.

I can't say I'm finished because I'm not completely satisfied with this result, but I stopped on this version.

Looks like I've fulfilled all conditions.
This implementation does not contain any optimisation for the multiplication algorithm, only full direct multiplication.

I'll be glad to get any feedback about it.

Best regards,
Misha